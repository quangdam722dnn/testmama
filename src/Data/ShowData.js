import React, { Component } from 'react';
import { View, Text, FlatList, Image, Animated ,TouchableHighlight} from 'react-native';
import FlatListData from '../Data/FlatListData';
import Carousel from 'react-native-snap-carousel'

class FlatListImage extends Component {
    render()
    {
        return(
            <View>
                <Image  
                style={{height:250,width:250}}
                source ={{uri:this.props.item.image}}
                />
            </View>
        );
    }
}

class FlatListItemName extends Component {
    render()
    {
        return(
            <View style={{marginLeft:10}}>
                <Text style={{fontWeight:'bold',color:'black',paddingTop:2}}>{this.props.item.name}</Text>
            </View>
        );
    }
}

class FlatListItem extends Component {
 render(){
        return (
          
        
            <View style={{
                flex: 1,
                width: 130,
                alignItems: 'center',
                backgroundColor: 'transparent',
                marginLeft: 10
            }}>
              
                <View>
                    <Image
                        source={{ uri: this.props.item.image }}
                        style={{ width: 100, height: 100, borderRadius: 10 }}
                    />
                </View>
                <View>
                    <Text ellipsizeMode='tail' numberOfLines={1}
                        style={{ fontSize: 17, color: 'black', fontWeight: 'bold', textAlign:'center', }}>{this.props.item.name}</Text>
                </View>
             
            </View>
         
        );
    }
}

export default class ShowData extends Component {
    constructor(props)
    {
        super(props);
    }
    render() {
       return(
           <View>
               <View style={{width:414,height:300,justifyContent:'center',alignItems:'center',backgroundColor:'green'}}>
               <Carousel
               autoplay={true}   
               autoplayDelay={2000}
               autoplayInterval={1000}  // can phuong thuc cai dat time ????
               layout={'default'}
               layoutCardOffset={`8`}
              ref={(c) => { this._carousel = c; }}
              data={FlatListData}
            
              sliderWidth={250}
              itemWidth={250}
              renderItem={({item,index}) => {
                  return(<FlatListImage item={item} index={index} ></FlatListImage>)
              }}
            />
            </View>
                <View style={{ backgroundColor:'transparent',flexDirection:'row',marginBottom:10,marginLeft:5,marginTop:5}}>
             <View style={{flex:1,borderRadius:10,backgroundColor:'black',justifyContent:'center',alignItems:'center'}}>
                 <Text style={{fontWeight:'bold',color:'white',padding:3}}>Đề xuất</Text>
             </View>
             <View style={{flex:6}}>
               <FlatList
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                data={FlatListData}
                renderItem={({ item, index }) => {
                    return (<FlatListItemName item={item} index={index} ></FlatListItemName>)
                }}
               >
                   
               </FlatList>
                 </View> 
             </View>          
            <FlatList
            showsHorizontalScrollIndicator={false}
                horizontal={true}
                data={FlatListData}
                renderItem={({ item, index }) => {
                    return (<FlatListItem
                        item={item} index={index}
                    >

                    </FlatListItem>)
                }}

            >
            </FlatList>
            </View>
        );
    }
}