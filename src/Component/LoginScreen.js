import React, { Component } from 'react';
import {
    View, Text, TextInput,
    TouchableHighlight, Image,
    StyleSheet, Dimensions,
    TouchableOpacity, Keyboard, TouchableWithoutFeedback,
    Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'


const Width = Dimensions.get('window').width;

const FBSDK = require('react-native-fbsdk');
const {
    LoginManager,
} = FBSDK;


export default class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            password: null,
        };
        this.login = this.login.bind(this);
        this.loginfb = this.loginfb.bind(this);
    
    }
    log()
    {
        this.props.navigation.navigte('Screen2');
    }
    loginfb() {
        LoginManager.logInWithPermissions(['public_profile']).then(
            function (result) {
                if (result.isCancelled) {
                    alert('Login was cancelled');
                } else {
                    this.props.navigation.push("Screen2")
                    // alert('Login was successful with permissions: '
                    //   + result.grantedPermissions.toString());
                }
            },
            function (error) {
                alert('Login failed with error: ' + error);
            }
        );
    }
   
    login() {
       
        if (this.state.email == 'blue' && this.state.password == 'blue') {
            return this.props.navigation.navigate('Screen2');
        } else if (this.state.email == null || this.state.password == null) {
            Alert.alert(' (*_* )', 'You need to enter your Email and Password')
        }
        else {
            Alert.alert('Account or Password is incorrect', '   Please try again !')
        }
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} >
                <View style={styles.container}>
                    <View
                        style={{
                            left: 0,
                            right: 0,
                            bottom: 0,
                            top: 0,
                            position: 'absolute'
                        }}>
                        <Image
                            source={{ uri: 'https://previews.123rf.com/images/nadianb/nadianb1708/nadianb170800105/83673140-cooking-mexican-taco-with-meat-beans-and-vegetables-at-black-stone-table-latin-american-food-backgro.jpg' }}
                            style={{
                                flex: 1,
                                justifyContent: 'center',
                                width: null,
                                height: null,
                                backgroundColor: 'transparent',
                                opacity: 0.5
                            }}
                        />
                    </View>

                    <Image
                        source={{ uri: 'http://cdn.marketplaceimages.windowsphone.com/v8/images/e2b12fa9-cf51-4142-9587-81cd89c3e1c8?imageType=ws_icon_large' }}
                        style={styles.image}
                    ></Image>

                    <TextInput style={styles.EmailandPassword} clearButtonMode='always' selectionColor='rgba(5, 250, 26, 1)'
                        keyboardType='email-address' placeholder='  Email'
                        onChangeText={text => this.setState({ email: text })}
                    />

                    <TextInput style={styles.EmailandPassword} selectionColor='rgba(5, 250, 26, 1)'
                        placeholder='  Password' secureTextEntry={true}
                        onChangeText={text => this.setState({ password: text })}
                    />

                    <TouchableHighlight style={styles.loginButton}
                        onPress={this.login}
                    >
                        <Text style={{ fontSize: 18, color: 'white', fontWeight: 'bold', textAlign: 'center', paddingTop: 9, paddingBottom: 9 }}>LOG IN</Text>
                    </TouchableHighlight>


                    <TouchableHighlight style={styles.forgotPassword}>
                        <Text style={{ fontSize: 18, color: 'white', textDecorationLine: 'underline', fontWeight: '100' }}>Forgot Password ? </Text>
                    </TouchableHighlight>


                    <View style={{ fex: 1, flexDirection: 'row', marginTop: 10 }}>
                        <View style={{ height: 1, backgroundColor: 'rgba(252, 106, 69, 1)', flex: 2, marginTop: 10, marginHorizontal: 20 }}></View>
                        <View style={{ flex: 1, backgroundColor: 'transparent' }}>
                            <Text style={{ color: 'rgba(255, 94, 0, 1)', textAlign: 'center', fontWeight: 'bold' }}>Log In With</Text>
                        </View>
                        <View
                            style={{ height: 1, backgroundColor: 'rgba(255, 94, 0, 1)', flex: 2, marginTop: 10, marginHorizontal: 20 }}></View>
                    </View>
                    <View
                        style={{ flex: 1, flexDirection: 'row' }}>

                        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center' }}
                            onPress={this.loginfb}
                        >
                            <Icon
                                name='logo-facebook'
                                size={30}
                                color='white'
                            />
                            <Text style={{ color: 'white', fontWeight: 'bold', paddingTop: 5 }}>  Facebook</Text>
                        </TouchableOpacity>

                        <View style={{ height: 1, width: Width / 3, backgroundColor: 'transparent' }}></View>

                        <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            <Icon
                                name='logo-google'
                                size={30}
                                color='white'
                            />
                            <Text style={{ color: 'white', fontWeight: 'bold', paddingTop: 5 }}>  Google</Text>
                        </TouchableOpacity>

                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }} >
                        <Text style={{ color: 'white', marginBottom: 0 }}>No Account ?  </Text>
                        <TouchableHighlight
                            style={{ backgroundColor: 'transparent', }}
                        >
                            <Text style={{ color: 'white', textDecorationLine: 'underline' }}>-Register now </Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </TouchableWithoutFeedback>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black'
    },
    image: {
        width: 150,
        height: 150,
        backgroundColor: 'transparent',
        marginTop: 80
    },
    EmailandPassword: {

        width: Width - 100,
        marginTop: 15,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: 'white'
    },
    loginButton: {
        marginTop: 22,
        borderRadius: 10,
        width: Width - 100,
        backgroundColor: 'rgba(252, 106, 69, 1)'
    },
    forgotPassword: {
        paddingTop: 12,
        backgroundColor: 'transparent',
    },

})
