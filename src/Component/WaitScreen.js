import React, { Component } from 'react';
import { View, Text, ActivityIndicator, Animated, StyleSheet, Image } from 'react-native';


export default class WaitScreen extends Component {
    constructor(props) {
        super(props);
        this.state={
            fadeView: new Animated.Value(0)
        };
       
    }
   componentWillMount() {
    setTimeout(() => {
        this.props.navigation.replace('Login')
    },0);

    setTimeout(() => {
        Animated.timing(this.state.fadeView,{
            toValue:1,
            duration:3000
        }).start();
        
    },0);
   }
    
    render() {
        return (
            <View style={styles.FullScreen}>
                <Image
                    style={{
                        top: 0,
                        bottom: 0,
                        width: null,
                        height: null,
                        right: 0,
                        left: 0,
                        position: 'absolute',

                    }}
                    source={{ uri: 'https://previews.123rf.com/images/catalby/catalby1408/catalby140800161/30889015-brown-wooden-background-with-stylized-waves-kitchen-utensils-and-empty-label-seashells-starfish-and-.jpg' }}
                ></Image>
                <View style={{justifyContent:'center',alignItems:'center'}}>
               <Animated.Image
               style={[styles.image,{opacity:this.state.fadeView}]}
               source={{ uri: 'https://actforfood.carrefour.com//storage/settings/September2018/ujc9YNOXFynvuhxwBx7v.png' }}>

               </Animated.Image>
               </View>
                <View style={{ alignItems: 'center', marginTop: 50 }}>
                    <Text style={{ fontSize: 27, color: 'rgba(69, 3, 176, 1)', fontWeight: 'bold', fontFamily: 'cursive', marginBottom: 10 }}>Welcome to Cooking MaMa</Text>
                    
                    <ActivityIndicator size="large" color="#0000ff" ></ActivityIndicator>

                    <Text style={{ fontSize: 20, color: 'rgba(69, 3, 176, 1)', fontWeight: 'bold', marginTop: 80, fontFamily: 'cursive' }}>Wish you will have a good experience !</Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end', marginBottom: 90, marginRight: 53 }}>
                    <Text style={{ fontSize: 13, fontWeight: 'bold', color: 'red' }}>Ver 1.1.0</Text>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    FullScreen: {
        flex: 1
    },
    image: {
        width: 150,
        height: 150,
        backgroundColor: 'transparent',
        
        marginTop:95
    }
});