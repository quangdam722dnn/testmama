import React,{Component} from 'react';
import {View,Text} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class ClassTab extends Component {
    static navigationOptions={
          
        tabBarIcon: ({tintColor}) => {
            return <Ionicons name='md-bookmarks' size={25} style={{color: tintColor}} />
        }
    }
    render()
    {
        return(
            <View style={{flex:1,justifyContent:'center',alignItems:'center' ,backgroundColor:'violet'}}>
                <Text style={{fontSize:20,color:'white',fontWeight:'bold'}}>Class Tab</Text>
            </View>
        );
    }

}