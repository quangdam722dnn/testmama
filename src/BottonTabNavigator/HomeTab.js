import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TextInput } from 'react-native-gesture-handler';

export default class HomeTab extends Component {
    static navigationOptions = {
        headerTitle: 'hahaha',
        tabBarIcon: ({ tintColor }) => {
            return <Ionicons name='md-home' size={25} style={{ color: tintColor }} />
        },

    }
    render() {
        return (
            <View >
                <View style={{ flexDirection: 'row', height: 45, justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ flex: 9, backgroundColor: 'transparent' }}>
                        <TouchableOpacity style={{ flexDirection: 'row', borderRadius: 10, backgroundColor: 'rgba(209, 209, 209, 1)', marginLeft: 8 }}>
                            <Ionicons name='md-search' size={25} color='black' style={{ marginLeft: 30, padding: 5 }} />
                            <Text style={{ padding: 5 }}>Search Gun </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 2 }}>
                        <TouchableOpacity>
                            <Ionicons name='md-notifications' size={33} color='black' style={{ marginLeft: 25 }} />
                        </TouchableOpacity>

                    </View>
                </View>
                <ScrollView>
                    <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: 'green' }}>
                        <Text style={styles.fontContainer}> Guns PUBG Mobile </Text>
                        <Text style={styles.bodyContainer} > AKM </Text>
                        <Image source={{ uri: "https://previews.123rf.com/images/snak/snak1803/snak180302474/98044231-ak-47-akm-assault-rifle.jpg", width: 400, height: 240 }} />
                        <Text style={styles.bodyContainer}> M416 </Text>
                        <Image source={{ uri: "https://upload.wikimedia.org/wikipedia/commons/9/97/HK416.jpg", width: 480, height: 200 }} />
                        <Text style={styles.bodyContainer}> UZI </Text>
                        <Image source={{ uri: "https://cdn.preppergunshop.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/l/o/logo_upp9s-th1_8397.jpg", width: 400, height: 200 }} />
                        <Text style={styles.bodyContainer}> K98 </Text>
                        <Image source={{ uri: "https://www.rockislandauction.com/html/dev_cdn/63/3323.jpg", width: 410, height: 100 }} />
                        <Text style={styles.bodyContainer}> AWM </Text>
                        <Image source={{ uri: "https://i.ytimg.com/vi/4nLBwd1xPrU/maxresdefault.jpg", width: 400, height: 200 }} />
                        <View style={{ marginBottom: 90 }}></View>
                    </View>
                </ScrollView>
            </View>
        );
    }

}
const styles = StyleSheet.create(
    {
        fontContainer: {
            fontSize: 30,
            color: 'blue',
            backgroundColor: 'yellow',
        },
        bodyContainer: {
            fontSize: 30,
            color: 'violet',
        },
        RightHeader: {

        }
    }
);