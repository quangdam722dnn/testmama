
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {createAppContainer,createStackNavigator,createSwitchNavigator,createBottomTabNavigator} from 'react-navigation'

import WaitScreen from './src/Component/WaitScreen';
import LoginScreen from './src/Component/LoginScreen';

import ClassTab from './src/BottonTabNavigator/ClassTab';
import CommunityTab from './src/BottonTabNavigator/CommunityTab';
import HomeTab from './src/BottonTabNavigator/HomeTab';
import PersonalTab from './src/BottonTabNavigator/PersonalTab';

const MAMA=createAppContainer(createSwitchNavigator({
  Screen1 :createStackNavigator({
    Splace: {screen : WaitScreen},
    Login: {screen: LoginScreen}
  },{
    initialRouteName:'Splace',
    headerMode:'none'
  }),
  Screen2 : createBottomTabNavigator({
    Home: {screen: HomeTab},
    Community :{screen : CommunityTab},
    Class : {screen: ClassTab},
    Personal : {screen: PersonalTab},

  }),
},{
  initialRouteName:'Screen1',
  headerMode:'none'
})
);

export default class App extends Component {
  render()
  {
    return<MAMA />;
  }
}